# W05 - Exam 1 Review / Preparation

* Review the Exam 1 study guide. 
* Propose 5 unique questions that would make great exam questions.[1-See important note below.] Your questions may be multiple-choice, true-false, or open-ended and involve coding. 

# To submit:

* Title your post "NN Lastname, Firstname - success" if successful.  Title your post "NN Lastname, Firstname" if you run into any problems or have any questions.  
* Share your five questions (and possible answers if multiple choice) as a numbered list.
* Do not provide the answers in your list.
* At the end of your post, provide all five enumerated answers. 
* Verify your answers are correct.  Incorrect answers may result in reduced points on the exam!
* [1] Important: You must personally generate five questions. As always, posts "highly similar" to earlier posts may receive a zero. Copying and pasting another entry does not warrant any credit - and worse, can be considered a serious violation of our academic integrity policy. The potential risk is not worth the time you save on these little assignments.)