1.To create a dropdown list in a web page, we use which of the following tags?

a) <ul>
b) <dropdown>
C) <select>
d) <option>

Ans: select

2.What is the difference between XML and HTML?

a) HTML is used for exchanging data, XML is not.
b) XML is used for exchanging data, HTML is not.
c) HTML can have user defined tags, XML cannot
d) Both b and c 

Ans: b

3. What attribute is used to specify number of rows?

a) Rownum
b) Rownumb
c) rn
d) Rowspan

Ans: Rowspan

4.Data cell can contain images.

a)True
b)False

Ans: True

5.Responsive web design uses what type of sizes?

A. Absolute
B. Relative

Ans: Relative

6. What are meta tags used for?

A. To store information usually relevant to browsers and search engines.
B. To only store information usually relevant to browsers
C. To only store information about search engines.
D. To store information about external links

Ans: A

7.What property is used to change the text color of an element?

A.fontcolor:
B.color:
C.font-color:
D.textcolor:

Ans: B

8. Which snippet of CSS is commonly used to center a website horizontally?

A.margin: 0 auto;
B.margin: auto 0;
C.margin: center;
D.site-align: center;

Ans:A

9.How do you make a list not display bullet points?

A.list-style-type: no-bullet
B.list: none
C.bulletpoints: none
D.list-style-type: none

Ans: D

10.What is the correct Syntax for importing a stylesheet in CSS?

A.@import-stylesheet url(css/example.css);
B.@import url(css/example.css);
C.@import-style url(css/example.css);
D.import-css url(css/example.css);

Ans: A

11.function() {
    var a = 10;
    if(a > 5) {
        a = 7;
    }
    alert(a);
}

A.7
B.10
C.NULL
D.UNDEFINED

Ans:A 

12.function getFunc() {
    var a = 7;
    return function(b) {
        alert(a+b);
    }
}
var f = getFunc();
f(5);

A.5
B.7
C.12
D.Undefined

Ans: C 

1.A
2.A
3.B
4.D
5.C